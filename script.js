/* Global settings */

const root = document.getElementById('root');

const boardWidth = 1000;
const boardHeight = 600;
const skyHeight = 60;
const squids = [];

// SCORES IN LOCAL STORAGE
let lastScores = JSON.parse(localStorage.getItem("lastScores")) || [];

const board = document.createElement('div');
board.setAttribute("id", "board");
board.style.width = `${boardWidth}px`;
board.style.height = `${boardHeight}px`;

const sky = document.createElement('div');
sky.setAttribute("id", "sky");
sky.style.height = `${skyHeight}px`

const infos = document.createElement('div');
infos.classList.add('infos');
const score = document.createElement('div');
const life = document.createElement('div');
score.innerText = "Score : 0";
life.innerText = "Vie : 5"
infos.append(score, life);
board.appendChild(infos);

board.append(sky);
root.appendChild(board);

/* SUBMARIN */

class Submarin {
    constructor() {
        this.positionX = 250;
        this.positionY = 250;
        this.width = 110;
        this.height = 60;
        this.life = 5;
        this.score = 0;

        this.submarin = document.createElement('div');
        this.submarin.setAttribute("id", "submarin");
        this.submarin.style.bottom = `${this.positionY}px`;
        this.submarin.style.left = `${this.positionX}px`;
        this.submarin.style.width = `${this.width}px`;
        this.submarin.style.height = `${this.height}px`;
        const submarinBody = document.createElement('div');
        submarinBody.setAttribute("id", "submarinBody");
        for (let i = 0; i < 3 ; i++) {
            let window = document.createElement('div');
            window.classList.add('windows');
            submarinBody.append(window);
        }
        const periscope = document.createElement('div');
        periscope.classList.add('periscope');
        const helix = document.createElement('div');
        helix.classList.add('helix');
        for (let i = 0; i < 4 ; i++) {
            let bubble = document.createElement('div');
            bubble.classList.add('bubbles');
            helix.append(bubble);
        }
        submarinBody.append(periscope, helix);
        this.submarin.appendChild(submarinBody);
        setTimeout(this.initialization, 2000);
    }

    initialization = () => {
        document.addEventListener('keydown', (event) => {
            if (event.code == 'ArrowUp') {
                if (this.positionY <= boardHeight - (skyHeight + this.height)) {
                    this.positionY = this.positionY + 5;
                    submarin.style.bottom = `${this.positionY}px`;
                }
            };
            if (event.code == 'ArrowDown') {
                if (this.positionY >= 5) {
                    this.positionY = this.positionY - 5;
                    submarin.style.bottom = `${this.positionY}px`;
                }
            };
            if (event.code == 'Space') {
                this.shoot()
            }
        });
    }

    getSubmarin() {
        return this.submarin;
    }

    looseLife() {
        this.life = this.life -1;
        life.innerText = `Vie : ${this.life}`;
        this.submarin.classList.add('danger');
        setTimeout(function(){ this.submarin.classList.remove('danger'); }, 300);
        if (this.life === 0) {
            this.gameOver();
        }
    }

    shoot() {
        let X = this.positionX + this.width;
        let Y = this.positionY + this.height / 2;
        const newBullet = new Bullet(X, Y);
        board.appendChild(newBullet.getBullet());
    }

    gameOver() {
        // update array of scores in local storage
        lastScores.push(this.score);
        localStorage.setItem("lastScores",  JSON.stringify(lastScores));
        //
        const gameover = document.createElement('div');
        gameover.style.position = "absolute";
        gameover.style.backgroundColor = "blue";
        gameover.style.borderRadius = "10px";
        gameover.style.color = "white";
        gameover.style.width = "300px";
        gameover.style.height = "200px";
        gameover.style.top = `${boardHeight/2 - 100}px`;
        gameover.style.left = `${boardWidth/2 - 150}px`;
        gameover.style.textAlign = "center";
        gameover.innerText = "Game Over";
        gameover.style.fontSize = "42px";
        gameover.style.zIndex = "999";
        const reload = document.createElement('button');
        reload.innerText = "Rejouer";
        reload.addEventListener('click', (event) => {
            window.location.reload();
        });
        gameover.appendChild(reload);
        board.appendChild(gameover);
    }

}


// BULLET

class Bullet {
    constructor(X, Y) {
        this.positionX = X;
        this.positionY = Y;
        this.width = 8;
        this.height = 8;
        this.collision = false;
        this.intervalID = 0;
        this.bullet = document.createElement('div');
        this.bullet.style.backgroundColor = "black";
        this.bullet.style.position = "absolute";
        this.bullet.style.bottom = `${this.positionY}px`;
        this.bullet.style.left = `${this.positionX}px`;
        this.bullet.style.width = `${this.width}px`;
        this.bullet.style.height = `${this.height}px`;
        this.bullet.style.borderRadius = "50%";
        this.initialization();
    }

    initialization = () => {
        this.intervalID = setInterval(() => {
            this.positionX += 5;
            this.bullet.style.left = `${this.positionX}px`;
            this.squidDetection();
            if (this.positionX > 1010) {
                clearInterval(this.intervalID)
                // board.removeChild(this.bullet);
            }
        }, 5);
    }

    getBullet() {
        return this.bullet;
    }

    squidDetection = () => {
        let potencialTargets = squids.filter(s => s.positionX <= boardWidth - (this.positionX + this.width))
        for (let index = 0; index < potencialTargets.length; index++) {
            const target = potencialTargets[index];
            if (( this.positionY - (target.positionY + target.height) < 0 ) 
            && (target.positionY - (this.positionY + this.height) < 0 )) {
                if ((boardWidth - (this.positionX + this.width)) - (target.positionX + target.width) < 0 ) {
                    if (!this.collision) {
                        target.dropDead();
                        this.bullet.style.backgroundColor = "transparent";
                        // TODO player get points
                        this.collision = true;
                    }
                }
            }
        }
    }
}

// SQUIDS

class Squid {
    constructor(Y, submarin) {
        this.positionX = -50;
        this.positionY = Y;
        this.collision = false;
        this.width = 50;
        this.height = 40;
        this.intervalID = 0;
        this.submarin = submarin;
        this.squid = document.createElement('div');
        this.squid.style.position = "absolute";
        this.squid.style.bottom = `${this.positionY}px`;
        this.squid.style.right = `${this.positionX}px`;
        this.squid.style.width = `${this.width}px`;
        this.squid.style.height = `${this.height}px`;
        this.squid.style.display = "flex";
        this.squid.style.alignItems = "center";
        const squidBody = document.createElement('div');
        const squidLegs = document.createElement('div');
        this.squid.append(squidBody, squidLegs);
        squidBody.classList.add('squid-body');
        squidLegs.classList.add('squid-legs');
        this.initialization();
    }

    initialization = () => {
        this.intervalID = setInterval(() => {
            this.positionX += 5;
            this.squid.style.right = `${this.positionX}px`;
            this.submarinDetection();
            if (this.positionX > 1010) {
                clearInterval(this.intervalID)
                // board.removeChild(this.squid);
            }
        }, 50);
    }

    getSquid() {
        return this.squid;
    }

    
    dropDead() {
        this.intervalID = setInterval(() => {
            this.positionY = this.positionY - 5;
            this.squid.style.bottom = `${this.positionY}px`;
            if (this.positionY < 0 - this.squid.height) {
                clearInterval(this.intervalID)
            }
        }, 2);
        this.submarin.score += 1
        score.innerText = `Score : ${this.submarin.score}`;
    }

    submarinDetection = () => {
        if (((boardWidth - (this.submarin.positionX + this.submarin.width)) - (this.positionX + this.width) < 0 ) && this.positionX - (boardWidth - this.submarin.positionX) < 0 ) {
            if (this.positionY < (this.submarin.positionY + this.submarin.height) && (this.positionY + this.height) > this.submarin.positionY ) {
                if (!this.collision) {
                    this.submarin.looseLife();
                    this.dropDead();
                    this.collision = true;
                  }
            }
        }
    }
}

// GOLD

class Gold {
    constructor(Y, submarin) {
        this.positionX = -50;
        this.positionY = Y;
        this.collision = false;
        this.width = 20;
        this.height = 20;
        this.intervalID = 0;
        this.submarin = submarin;
        this.gold = document.createElement('div');
        this.gold.style.position = "absolute";
        this.gold.style.bottom = `${this.positionY}px`;
        this.gold.style.right = `${this.positionX}px`;
        this.gold.style.width = `${this.width}px`;
        this.gold.style.height = `${this.height}px`;
        this.gold.classList.add('gold');
        const coin = document.createElement('div');
        coin.innerText = "$"
        this.gold.appendChild(coin);
        coin.classList.add('coin');
        this.initialization();
    }

    initialization = () => {
        this.intervalID = setInterval(() => {
            this.positionX += 3;
            this.gold.style.right = `${this.positionX}px`;
            this.submarinDetection();
            if (this.positionX > 1010) {
                clearInterval(this.intervalID);
            }
        }, 50);
    }

    getGold() {
        return this.gold;
    }

    submarinDetection = () => {
        if (((boardWidth - (this.submarin.positionX + this.submarin.width)) - (this.positionX + this.width) < 0 ) && this.positionX - (boardWidth - this.submarin.positionX) < 0 ) {
            if (this.positionY < (this.submarin.positionY + this.submarin.height) && (this.positionY + this.height) > this.submarin.positionY ) {
                if (!this.collision) {
                    this.submarin.score += 10;
                    score.innerText = `Score : ${this.submarin.score}`;
                    this.gold.classList.add('animated');
                    this.collision = true;
                  }
            }
        }
    }
}

// Initialisation du jeu

const newSubmarin = new Submarin();
board.appendChild(newSubmarin.getSubmarin());

function generateSquid() {
    setInterval(() => {
        const value = boardHeight - skyHeight - 100
        const randomY = Math.floor(Math.random() * value);
        const newSquid = new Squid(randomY, newSubmarin);
        board.appendChild(newSquid.getSquid());
        squids.push(newSquid)
    }, 2000);
}
generateSquid();


function generateGold() {
    setInterval(() => {
        const value = boardHeight - skyHeight - 100
        const randomY = Math.floor(Math.random() * value);
        const newGold = new Gold(randomY, newSubmarin);
        board.appendChild(newGold.getGold());
    }, 8000);
}
generateGold();